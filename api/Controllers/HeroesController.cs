using System.Collections.Generic;
using System.Linq;
using api.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace api.Controllers
{
  [Route("api/[controller]")]
  public class HeroesController : Controller
  {
    private IMemoryCache _cache;

    private static readonly string HeroesCacheKey = "heroes";
    private readonly Hero[] Heroes;

    public HeroesController(IMemoryCache memoryCache)
    {
      _cache = memoryCache;
      Heroes = GetHeroesFromCacheOrInitializeIt();
    }

    // GET: api/heroes
    [HttpGet]
    public IEnumerable<Hero> Get(string name)
    {
      if (!string.IsNullOrWhiteSpace(name))
      {
        return Heroes.Where(hero => hero.name.ToLower().Contains(name.ToLower()));
      }

      return Heroes;
    }

    // GET: api/heroes/5
    [HttpGet("{id}")]
    public ActionResult<Hero> Get(int id)
    {
      var hero = GetHeroById(id);
      if (hero == null)
        return new NotFoundResult();

      return hero;
    }

    // PUT: api/heroes
    [HttpPut]
    public ActionResult Put([FromBody]Hero hero)
    {
      var foundHero = GetHeroById(hero.id);
      if (hero == null)
        return new NotFoundResult();

      foundHero.name = hero.name;

      SetHeroesInCache(Heroes);

      return new EmptyResult();
    }

    // POST: api/heroes
    [HttpPost]
    public Hero Post([FromBody]Hero newHero)
    {
      int lastId = Heroes.Select(hero => hero.id).Max();
      newHero.id = lastId + 1;

      var updatedHeroesList = new List<Hero>(Heroes)
      {
        newHero
      };
      SetHeroesInCache(updatedHeroesList.ToArray());

      return newHero;
    }

    // DELETE: api/heroes/5
    [HttpDelete("{id}")]
    public void Delete(int id)
    {
      var updatedHeroesList = Heroes.Where(hero => hero.id != id);
      SetHeroesInCache(updatedHeroesList.ToArray());
    }

    private Hero GetHeroById(int id)
    {
      return Heroes.FirstOrDefault(hero => hero.id == id);
    }

    private Hero[] GetHeroesFromCacheOrInitializeIt()
    {
      return _cache.GetOrCreate(HeroesCacheKey, (cache) => new Hero[]
      {
        new Hero { id = 11, name = "Mr. Nice" },
        new Hero { id = 12, name = "Narco" },
        new Hero { id = 13, name = "Bombasto" },
        new Hero { id = 14, name = "Celeritas" },
        new Hero { id = 15, name = "Magneta" },
        new Hero { id = 16, name = "RubberMan" },
        new Hero { id = 17, name = "Dynama" },
        new Hero { id = 18, name = "Dr IQ" },
        new Hero { id = 19, name = "Magma" },
        new Hero { id = 20, name = "Tornado" }
      });
    }

    private void SetHeroesInCache(Hero[] heroes)
    {
      _cache.Set(HeroesCacheKey, heroes);
    }
  }
}
