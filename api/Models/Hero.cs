using System;

namespace api.Models
{
  public class Hero
  {
    public int id { get; set; }
    public string name { get; set; }
  }
}
